import java.util.Scanner;

public class App {
    static Map map = new Map();
    static Vehicle BenZ = new Vehicle(map, "BenZ", 'B', 5, 9);
    static Vehicle Audi = new Vehicle(map, "Audi", 'O', 1, 6);
    static Vehicle Honda = new Vehicle(map, "Honda", 'H', 43, 6);
    static Store Store1 = new Store(map, "Store1",'B', 25, 5);
    static Store Store2 = new Store(map, "Store2",'H', 10, 5);
    static Store Store3 = new Store(map, "Store3",'O', 35, 5);
    static Scanner sc = new Scanner(System.in);
    public static String input(){
        return sc.next();
    }
    public static void BenZControl(String command){
       switch(command){
           case "w":
               BenZ.Up();
               break;
           case "s":
               BenZ.Down();
               break;
           case "a":
                BenZ.Left();
               break;
           case "d":
                BenZ.Right();
               break;
           case "q":
               System.exit(0);
               break;
           default:
               break;
       }
    }
    public static void AudiControl(String command){
        switch(command){
            case "w":
                Audi.Up();
                break;
            case "s":
                Audi.Down();
                break;
            case "a":
                Audi.Left();
                break;
            case "d":
                Audi.Right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
     }
     public static void HondaControl(String command){
        switch(command){
            case "w":
                Honda.Up();
                break;
            case "s":
                Honda.Down();
                break;
            case "a":
                Honda.Left();
                break;
            case "d":
                Honda.Right();
                break;
            case "q":
                System.exit(0);
                break;
            default:
                break;
        }
     }

    public static void checkP() {
        if(BenZ.Getsymbol() == Store1.Getsymbol() && BenZ.GetX() == Store1.GetX() && BenZ.GetY() == Store1.GetY()){
            Store1.succ();
            BenZ.succ();
            map.add(Audi);
        }
        if(Audi.Getsymbol() == Store3.Getsymbol() && Audi.GetX() == Store3.GetX() && Audi.GetY() == Store3.GetY()){
            Audi.succ();
            Store3.succ();
            map.add(Honda);
        }
        if(Honda.Getsymbol() == Store2.Getsymbol() && Honda.GetX() == Store2.GetX() && Honda.GetY() == Store2.GetY()){
            Honda.succ();
            Store2.succ();
            
        }
        
    }
    

    
    public static void main(String[] args) throws Exception {



     //   map.add(Audi);
        map.add(Store1);
        map.add(Store2);
        map.add(Store3);
        map.add(BenZ);

        while(true){
            map.print();
            if(BenZ.symbol == 'X' && Audi.symbol == 'X' && Honda.symbol == 'X'){
                break;
            }
            String command = input();
            if(BenZ.symbol != 'X'){
                BenZControl(command);
            }
            
            checkP();
            if(BenZ.symbol == 'X' && Audi.symbol != 'X'){
                AudiControl(command);
                checkP();
            }
            if(Audi.symbol == 'X'){
                HondaControl(command);
                checkP();
            }

        }
    }
    
}