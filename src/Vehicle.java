public class Vehicle extends Unit{

    public Vehicle(Map map, String name, char symbol, int x, int y) {
        super(map, name, symbol, x, y);
        //TODO Auto-generated constructor stub
    }
    public boolean Up(){
        if(this.y == 1) return false;
        this.y--;
        return true;
    }
    public boolean Down(){
        if(this.y == 10) return false;
        this.y++;
        return true;
    }
    public boolean Left(){
        if(this.x == 1) return false;
        this.x--;
        return true;
    }
    public boolean Right(){
        if(this.x == 50) return false;
        this.x++;
        return true;
    }
    
}
