public class Unit {
    public char symbol;
    private String name;
    public int x;
    public int y;
    private Map map;


    public Unit(Map map,String name,char symbol,int x, int y){
        this.map = map;
        this.name = name;
        this.symbol = symbol;
        this.x =x;
        this.y =y;
    }
    public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }

    public void succ(){
        this.symbol = 'X';
    }

    public int GetX(){
        return x;
    }

    public int GetY(){
        return y;
    }

    public char Getsymbol(){
        return symbol;
    }

    public String Getname(){
        return name;
    }


    public String toString(){
        return "Car Name :"+ this.name +" is On Map"+map.toString();


}
